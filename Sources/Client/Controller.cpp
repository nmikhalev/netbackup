#include <boost/asio.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>

#include "../Shared/ByteArray.h"
#include "../Shared/Exceptions.h"
#include "../Shared/FileHash.h"
#include "../Shared/FileReader.h"
#include "../Shared/NetworkProtocol/Agreements.h"
#include "../Shared/NetworkProtocol/NetBackup.pb.h"
#include "../Shared/NetworkProtocol/Packet.h"
#include "../Shared/Logger.h"
#include "../Shared/StringConverter.h"
#include "../Shared/TcpSocket.h"
#include "../Shared/Workflow.h"
#include "Controller.h"
#include "FileInfo.h"
#include "FileEnumerator.h"
#include "TransmittedFile.h"

namespace NetBackup
{
    using namespace NetworkProtocol;

    Controller::Controller(boost::asio::io_service& ioService) :
        ioService(ioService)
    {
    }

    void Controller::start(const std::shared_ptr<Settings>& settings)
    {
        m_settings = settings;
        Logger::info(std::wstring(L"ClientID: ") + toWString(m_settings->clientId));

        using namespace boost::asio::ip;
        serverEndpoint = tcp::endpoint(address_v4::from_string(m_settings->serverIp), m_settings->serverPort);
        connectToServer();
    }

    void Controller::connectToServer()
    {
        Logger::info(boost::format("Connecting to server %1%:%2%...") %
                     serverEndpoint.address().to_string() %
                     serverEndpoint.port());

        auto serverSocket = std::make_shared<TcpSocket>(ioService);
        serverSocket->asyncConnect(
            serverEndpoint,
            boost::bind(&Controller::onServerConnected, shared_from_this(),
                        boost::asio::placeholders::error,
                        serverSocket));
    }

    void Controller::onServerConnected(const boost::system::error_code& connectionError,
                                       SharedTcpSocket serverSocket)
    {
        try
        {
            if (connectionError)
                throw ConnectionFailure("Failed to connect to the server: ", connectionError.message());

            Logger::info("Connected to the server");

            filesTransmitted = 0;
            rejectedFiles.clear();
            fileSource = std::make_unique<FileEnumerator>(
                m_settings->backupEntries.cbegin(),
                m_settings->backupEntries.cend());

            boost::system::error_code error;
            InitialRequest initialRequest;
            initialRequest.set_clientid(m_settings->clientId);
            auto serializedRequest = initialRequest.SerializeAsString();
            auto packet = Packet::create(Packet::kInitialRequest, serializedRequest);
            serverSocket->send(boost::asio::buffer(packet.data(), packet.size()), error);
            if (error)
                throw ConnectionFailure("Failed to send initial request: ", error.message());

            auto receiveBuffer = makeSharedByteArray(0);
            listenServer(serverSocket, receiveBuffer, nullptr);
        }
        catch (const ConnectionFailure& e)
        {
            Logger::error(e.what());
            processConnectionFailureWithoutExceptions();
        }
        catch (const std::exception& e)
        {
            Logger::error(std::string(NETBACKUP_PRETTY_FUNCTION) + " : " + e.what());
        }
    }

    void Controller::listenServer(const SharedTcpSocket& serverSocket,
                                  const SharedByteArray& receiveBuffer,
                                  const SharedTransmittedFile& file)
    {
        receiveBuffer->resize(sizeof(Packet::Size));
        serverSocket->asyncReceive(
            boost::asio::buffer(receiveBuffer->data(), receiveBuffer->size()),
            boost::bind(&Controller::onPacketSizeReceived, shared_from_this(),
                        boost::asio::placeholders::error,
                        serverSocket, receiveBuffer, file));
    }

    void Controller::onPacketSizeReceived(const boost::system::error_code& error,
                                          SharedTcpSocket serverSocket,
                                          SharedByteArray receiveBuffer,
                                          SharedTransmittedFile file)
    {
        try
        {
            if (error)
                throw ConnectionFailure("Failed to receive size of packet: ", error.message());

            auto totalSize = *reinterpret_cast<const Packet::Size*>(receiveBuffer->data());
            receiveBuffer->resize(totalSize, ByteArray::PreserveContent::Yes);

            serverSocket->asyncReceive(
                boost::asio::buffer(receiveBuffer->data() + sizeof(totalSize),
                                    receiveBuffer->size() - sizeof(totalSize)),
                boost::bind(&Controller::onPacketReceived, shared_from_this(),
                            boost::asio::placeholders::error,
                            serverSocket, receiveBuffer, file));
        }
        catch (const ConnectionFailure& e)
        {
            Logger::error(e.what());
            processConnectionFailureWithoutExceptions();
        }
        catch (const std::exception& e)
        {
            Logger::error(std::string(NETBACKUP_PRETTY_FUNCTION) + " : " + e.what());
        }
    }

    void Controller::onPacketReceived(const boost::system::error_code& error,
                                      SharedTcpSocket serverSocket,
                                      SharedByteArray receiveBuffer,
                                      SharedTransmittedFile file)
    {
        try
        {
            if (error)
                throw ConnectionFailure("Failed to receive packet: ", error.message());

            auto packetType = Packet::typeOf(*receiveBuffer);
            switch (packetType)
            {
            case Packet::kInitialResponse:
                processInitialResponse(serverSocket, receiveBuffer);
                break;
            case Packet::kUploadFileResponse:
                processUploadFileResponse(serverSocket, receiveBuffer, file);
                break;
            case Packet::kSavePayloadResponse:
                processSavePayloadResponse(serverSocket, receiveBuffer, file);
                break;
            default:
                throw std::runtime_error(std::string("Received unknown packet: ") + std::to_string(packetType));
            }
        }
        catch (const ConnectionFailure& e)
        {
            Logger::error(e.what());
            processConnectionFailureWithoutExceptions();
        }
        catch (const std::exception& e)
        {
            Logger::error(std::string(NETBACKUP_PRETTY_FUNCTION) + " : " + e.what());
        }
    }

    void Controller::processInitialResponse(const SharedTcpSocket& serverSocket,
                                            const SharedByteArray& receivedPacket)
    {
        InitialResponse initialResponse;
        auto packetPayload = Packet::rawPayloadOf(*receivedPacket);
        if (!initialResponse.ParseFromArray(packetPayload.first, packetPayload.second))
            throw std::runtime_error("Excepted initial response, but received something else");

        auto serverProtocolVersion = initialResponse.protocolversion();
        Logger::info(std::string("Server protocol version: ") + std::to_string(serverProtocolVersion));
        if (serverProtocolVersion != NetworkProtocol::kVersion)
            throw std::logic_error("Server use another protocol version");

        transmitNextFile(serverSocket, receivedPacket);
    }

    void Controller::processUploadFileResponse(const SharedTcpSocket& serverSocket,
                                               const SharedByteArray& receivedPacket,
                                               const SharedTransmittedFile& file)
    {
        UploadFileResponse uploadResponse;
        auto packetPayload = Packet::rawPayloadOf(*receivedPacket);
        if (!uploadResponse.ParseFromArray(packetPayload.first, packetPayload.second))
            throw std::runtime_error("Failed to unpack response to upload file");

        if (uploadResponse.isconfirmed())
        {
            auto& fileInfo = file->info;
            NETBACKUP_PRECONDITION(fileInfo);
            file->reader = std::make_unique<FileReader>(fileInfo->path());
            transmitNextFileChunk(serverSocket, receivedPacket, file);
        }
        else
        {
            if (uploadResponse.has_isuploaded() && uploadResponse.isuploaded())
            {
                Logger::info(L"File has been already uploaded");
            }
            else
            {
                rejectedFiles.emplace_back(file->info->path());
                std::string rejectionCause("Unknown case");
                if ((uploadResponse.has_rejectioncause()))
                    rejectionCause = uploadResponse.rejectioncause();
                Logger::error(std::string("Failed to upload file: ") + rejectionCause);
            }
            transmitNextFile(serverSocket, receivedPacket);
        }
    }

    void Controller::processSavePayloadResponse(const SharedTcpSocket& serverSocket,
                                                const SharedByteArray& receivedPacket,
                                                const SharedTransmittedFile& file)
    {
        SavePayloadResponse response;
        auto packetPayload = Packet::rawPayloadOf(*receivedPacket);
        if (!response.ParseFromArray(packetPayload.first, packetPayload.second))
            throw std::runtime_error("Failed to unpack response after saving payload");

        if (response.ispayloadsaved())
        {
            transmitNextFileChunk(serverSocket, receivedPacket, file);
        }
        else
        {
            const char* errorDescription = "Server didn't send cause of the error";
            if (response.has_errordescription())
                errorDescription = response.errordescription().c_str();
            throw std::runtime_error(std::string("Server could not save payload: ") + errorDescription);
        }
    }

    void Controller::transmitNextFileChunk(const SharedTcpSocket& serverSocket,
                                           const SharedByteArray& receiveBuffer,
                                           const SharedTransmittedFile& file)
    {
        auto& fileReader = file->reader;
        NETBACKUP_PRECONDITION(fileReader);
        auto& readBuffer = file->readBuffer;
        readBuffer.resize(m_settings->maxTransmittedChunkSizeMB * 1024 * 1024);
        auto bytesRead = fileReader->readSome(readBuffer.data(), readBuffer.size());
        if (bytesRead > 0)
        {
            SavePayloadRequest request;
            request.set_payload(readBuffer.data(), bytesRead);
            auto serializedRequest = request.SerializeAsString();
            auto packet = Packet::createShared(Packet::kSavePayloadRequest, serializedRequest);
            serverSocket->asyncSend(
                boost::asio::buffer(packet->data(), packet->size()),
                boost::bind(&Controller::onFileChunkTransmitted, shared_from_this(),
                            boost::asio::placeholders::error,
                            serverSocket, receiveBuffer, file, packet));
        }
        else
        {
            ++filesTransmitted;
            Logger::info("The file has been transmitted");
            transmitNextFile(serverSocket, receiveBuffer);
        }
    }

    void Controller::onFileChunkTransmitted(const boost::system::error_code& error,
                                            SharedTcpSocket serverSocket,
                                            SharedByteArray receiveBuffer,
                                            SharedTransmittedFile file,
                                            SharedByteArray)
    {
        try
        {
            if (error)
                throw ConnectionFailure("Failed to send file chunk: ", error.message());
            listenServer(serverSocket, receiveBuffer, file);
        }
        catch (const ConnectionFailure& e)
        {
            Logger::error(e.what());
            processConnectionFailureWithoutExceptions();
        }
        catch (const std::exception& e)
        {
            Logger::error(std::string(NETBACKUP_PRETTY_FUNCTION) + " : " + e.what());
        }
    }

    void Controller::transmitNextFile(const SharedTcpSocket& serverSocket,
                                      const SharedByteArray& receiveBuffer)
    {
        auto fileInfo = fileSource->fetchNextFileInfo();
        if (fileInfo)
        {
            auto filePath = fileInfo->path().generic_wstring();
            Logger::info(boost::wformat(L"Processing: %1% (%2%)") % filePath % fileInfo->prettySize());

            UploadFileRequest request;
            auto serializedFilePath = toUtf8(filePath);
            request.set_filepath(serializedFilePath.data(), serializedFilePath.size());
            request.set_filesize(fileInfo->size());
            auto& fileHash = fileInfo->hash();
            request.set_filehash(fileHash.data(), fileHash.size());

            boost::system::error_code connectionError;
            auto serializedRequest = request.SerializeAsString();
            auto packet = Packet::createShared(Packet::kUploadFileRequest, serializedRequest);
            serverSocket->send(boost::asio::buffer(packet->data(), packet->size()), connectionError);
            if (connectionError)
                throw ConnectionFailure(std::string("Failed to send request for uploading file: ") + connectionError.message());

            auto file = std::make_shared<TransmittedFile>(std::move(fileInfo));
            listenServer(serverSocket, receiveBuffer, file);
        }
        else
        {
            completeTransmission(serverSocket);
        }
    }

    void Controller::completeTransmission(const SharedTcpSocket& serverSocket)
    {
        boost::system::error_code dummyError;
        auto packet = Packet::create(Packet::kFinishNotification, std::string());
        serverSocket->send(boost::asio::buffer(packet.data(), packet.size()), dummyError);

        Logger::info(boost::format("Transmitted %1% files") % filesTransmitted);
        if (rejectedFiles.empty())
        {
            Logger::info(L"Completed successfully!");
        }
        else
        {
            for (const auto& rejectedFile : rejectedFiles)
                Logger::error(std::wstring(L"Rejected: ") + rejectedFile.generic_wstring());
            Logger::warning("Completed with errors");
        }
    }

    void Controller::processConnectionFailureWithoutExceptions()
    {
        try
        {
            Logger::info(boost::format("Reconnection to the server in %1% seconds...") % kReconnectionIntervalInSeconds);

            auto reconnectionTimer = std::make_shared<boost::asio::deadline_timer>(ioService);
            reconnectionTimer->expires_from_now(boost::posix_time::seconds(kReconnectionIntervalInSeconds));
            reconnectionTimer->async_wait(
                boost::bind(&Controller::onTimeToReconnectServer, shared_from_this(),
                            boost::asio::placeholders::error,
                            reconnectionTimer));
        }
        catch (const std::exception& e)
        {
            Logger::error("Failed to reconnect to the server: ", e.what());
        }
    }

    void Controller::onTimeToReconnectServer(const boost::system::error_code& error,
                                             SharedDeadlineTimer)
    {
        try
        {
            if (error != boost::asio::error::operation_aborted)
                connectToServer();
        }
        catch (const std::exception& e)
        {
            Logger::error("Failed to reconnect server: ", e.what());
        }
    }
}
