#pragma once

#include <boost/asio/ip/tcp.hpp>
#include <memory>
#include <string>

#include "../Shared/ByteArray.h"
#include "../Shared/NetworkPort.h"
#include "../Shared/FileReader.h"
#include "Settings.h"

namespace NetBackup
{
    class ByteArray;
    class FileInfo;
    class FileEnumerator;
    class TcpSocket;
    class TransmittedFile;

    class Controller : public std::enable_shared_from_this<Controller>
    {
    public:
        using SharedFileInfo = std::shared_ptr<FileInfo>;
        using SharedByteArray = std::shared_ptr<ByteArray>;
        using SharedTcpSocket = std::shared_ptr<TcpSocket>;
        using SharedDeadlineTimer = std::shared_ptr<boost::asio::deadline_timer>;
        using SharedTransmittedFile = std::shared_ptr<TransmittedFile>;

    private:
        const int kReconnectionIntervalInSeconds = 60;

    public:
        std::shared_ptr<Settings> m_settings;

        boost::asio::io_service& ioService;
        boost::asio::ip::tcp::endpoint serverEndpoint;

        std::unique_ptr<FileEnumerator> fileSource;
        std::list<boost::filesystem::path> rejectedFiles;
        size_t filesTransmitted;

    public:
        Controller(boost::asio::io_service& ioService);

    public:
        void start(const std::shared_ptr<Settings>& settings);

    public:
        void onFileChunkTransmitted(
                const boost::system::error_code& error,
                SharedTcpSocket serverSocket,
                SharedByteArray receiveBuffer,
                SharedTransmittedFile file,
                SharedByteArray sentPacket);
        void onPacketReceived(
                const boost::system::error_code& error,
                SharedTcpSocket serverSocket,
                SharedByteArray receiveBuffer,
                SharedTransmittedFile file);
        void onPacketSizeReceived(
                const boost::system::error_code& error,
                SharedTcpSocket serverSocket,
                SharedByteArray receiveBuffer,
                SharedTransmittedFile file);
        void onServerConnected(
                const boost::system::error_code& error,
                SharedTcpSocket serverSocket);
        void onTimeToReconnectServer(
                const boost::system::error_code& error,
                SharedDeadlineTimer);
        void onUpdloadFileRequestSent(
                const boost::system::error_code& error,
                SharedTcpSocket serverSocket,
                SharedFileInfo fileInfo,
                SharedByteArray sentPacket);

    private:
        void connectToServer();
        void completeTransmission(
                const SharedTcpSocket& serverSocket);
        void listenServer(
                const SharedTcpSocket& serverSoket,
                const SharedByteArray& receiveBuffer,
                const SharedTransmittedFile& file);
        void processConnectionFailureWithoutExceptions();
        void processInitialResponse(
                const SharedTcpSocket& serverSocket,
                const SharedByteArray& receivedPacket);
        void processSavePayloadResponse(
                const SharedTcpSocket& serverSocket,
                const SharedByteArray& receivedPacket,
                const SharedTransmittedFile& file);
        void processUploadFileResponse(
                const SharedTcpSocket& serverSocket,
                const SharedByteArray& receivedPacket,
                const SharedTransmittedFile& file);
        void transmitNextFile(
                const SharedTcpSocket& serverSocket,
                const SharedByteArray& receiveBuffer);
        void transmitNextFileChunk(
                const SharedTcpSocket& serverSocket,
                const SharedByteArray& receiveBuffer,
                const SharedTransmittedFile& file);
    };
}
