#include "../Shared/Destructor.h"
#include "FileInfo.h"
#include "FileEnumerator.h"

namespace NetBackup
{
    FileEnumerator::~FileEnumerator()
    {
        isDestructorCalled = true;
        if (backgroundEnumarator.joinable())
            backgroundEnumarator.join();
    }

    void FileEnumerator::backgroundEnumeratorRoutine()
    {
        Destructor onPrefetchingCompleted([&]()
        {
            isPrefetchingInProgress = false;
        });

        size_t filesToPrefetch = 0;
        while (!isDestructorCalled)
        {
            if (filesToPrefetch == 0)
            {
                std::lock_guard<std::mutex> prefetchedFilesLocker(prefetchedFilesMutex);
                if (prefetchedFiles.size() < kMaxPrefetchedFiles)
                    filesToPrefetch = kMaxPrefetchedFiles - prefetchedFiles.size();
            }

            if (filesToPrefetch > 0)
            {
                auto fileInfo = fetchNextFileInfoWithoutCache();
                if (fileInfo)
                {
                    std::lock_guard<std::mutex> prefetchedFilesLocker(prefetchedFilesMutex);
                    prefetchedFiles.emplace_back(std::move(fileInfo));
                }
                else break;
                --filesToPrefetch;
            }
            else std::this_thread::sleep_for(std::chrono::seconds(5));
        }
    }

    std::unique_ptr<FileInfo> FileEnumerator::fetchNextFileInfoWithoutCache()
    {
        while (true)
        {
            using namespace boost;
            if (directoryIterator == kDirectoryIteratorEnd)
            {
                if (!originalEntries.empty())
                {
                    auto originalEntryIterator = originalEntries.begin();
                    filesystem::path originalEntry(*originalEntryIterator);
                    originalEntries.erase(originalEntryIterator);

                    if (filesystem::is_regular_file(originalEntry))
                        return std::make_unique<FileInfo>(originalEntry);
                    else if (filesystem::is_directory(originalEntry))
                        directoryIterator = filesystem::recursive_directory_iterator(originalEntry);
                }
                else break;
            }

            while (directoryIterator != kDirectoryIteratorEnd)
            {
                if (filesystem::is_regular_file(*directoryIterator))
                {
                    auto fileInfo = std::make_unique<FileInfo>(*directoryIterator);
                    ++directoryIterator;
                    return fileInfo;
                }
                ++directoryIterator;
            }
        }
        return std::unique_ptr<FileInfo>();
    }

    std::unique_ptr<FileInfo> FileEnumerator::fetchNextFileInfo()
    {
        std::unique_ptr<FileInfo> fileInfo;
        while (true)
        {
            std::unique_lock<std::mutex> prefetchedFilesLocker(prefetchedFilesMutex);
            if (!prefetchedFiles.empty())
            {
                fileInfo = std::move(prefetchedFiles.front());
                prefetchedFiles.pop_front();
                break;
            }
            else
            {
                prefetchedFilesLocker.unlock();
                if (isPrefetchingInProgress)
                    std::this_thread::sleep_for(std::chrono::seconds(1));
                else
                    break;
            }
        }
        return fileInfo;
    }
}
