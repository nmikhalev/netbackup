#pragma once

#include <atomic>
#include <list>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

#include <boost/filesystem.hpp>

namespace NetBackup
{
    class FileInfo;

    class FileEnumerator
    {
    private:
        static const auto kMaxPrefetchedFiles = 1024;

    private:
        const boost::filesystem::recursive_directory_iterator kDirectoryIteratorEnd;

    private:
        std::atomic<bool> isDestructorCalled { false };
        std::list<boost::filesystem::path> originalEntries;
        boost::filesystem::recursive_directory_iterator directoryIterator;

        std::atomic<bool> isPrefetchingInProgress { true };
        std::list<std::unique_ptr<FileInfo>> prefetchedFiles;
        std::mutex prefetchedFilesMutex;
        std::thread backgroundEnumarator;

    public:
        template<typename Iterator>
        FileEnumerator(const Iterator& fileSystemEntriesBegin,
                       const Iterator& fileSystemEntriesEnd) :
            originalEntries(fileSystemEntriesBegin, fileSystemEntriesEnd),
            directoryIterator(kDirectoryIteratorEnd),
            backgroundEnumarator(&FileEnumerator::backgroundEnumeratorRoutine, this)
        {
        }

        ~FileEnumerator();

    public:
        std::unique_ptr<FileInfo> fetchNextFileInfo();

    private:
        void backgroundEnumeratorRoutine();
        std::unique_ptr<FileInfo> fetchNextFileInfoWithoutCache();
    };
}
