#include <iomanip>
#include <sstream>

#include "../Shared/FileHash.h"
#include "FileInfo.h"

namespace NetBackup
{
    constexpr auto kBytesInKByte = 1024;
    constexpr auto kBytesInMByte = 1024 * kBytesInKByte;
    constexpr auto kBytesInGByte = 1024 * kBytesInMByte;

    FileInfo::FileInfo(const boost::filesystem::path& path) :
        m_path(path),
        m_size(boost::filesystem::file_size(m_path)),
        m_hash(FileHash::computeMD5(m_path))
    {
    }

    std::wstring FileInfo::prettySize() const
    {
        const wchar_t* suffix = L"";
        float normalizedSize = 0;
        auto numericSize = size();
        if (numericSize < kBytesInKByte)
        {
            normalizedSize = numericSize;
            suffix = L"B";
        }
        else if (numericSize < kBytesInMByte)
        {
            normalizedSize = numericSize / 1024.0f;
            suffix = L"KB";
        }
        else if (numericSize < kBytesInGByte)
        {
            normalizedSize = numericSize / (1024.0f * 1024.0f);
            suffix = L"MB";
        }
        else
        {
            normalizedSize = numericSize / (1024.0f * 1024.0f * 1024.0f);
            suffix = L"GB";
        }

        std::wstringstream stream;
        stream << std::setprecision(3) << normalizedSize << ' ' << suffix;
        return stream.str();
    }
}
