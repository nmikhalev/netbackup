#pragma once

#include <boost/filesystem.hpp>
#include <boost/optional.hpp>

#include "../Shared/ByteArray.h"
#include "../Shared/FileHash.h"

namespace NetBackup
{
    class FileInfo
    {
    private:
        boost::filesystem::path m_path;
        uint64_t m_size;
        ByteArray m_hash;

    public:
        FileInfo(const boost::filesystem::path& path);

    public:
        const boost::filesystem::path& path() const
        {
            return m_path;
        }

        uint64_t size() const
        {
            return m_size;
        }

        const ByteArray& hash()
        {
            return m_hash;
        }

        std::wstring prettySize() const;
    };
}
