#include <codecvt>

#include <boost/format.hpp>
#include <tinyxml2.h>

#include "../Shared/FileReader.h"
#include "../Shared/StringConverter.h"
#include "Settings.h"

namespace NetBackup
{
    Settings::Settings(const boost::filesystem::path& filePath)
    {
        using namespace tinyxml2;
        auto rawConfiguration = FileReader::readAll(filePath);
        XMLDocument xmlDocument;
        auto xmlError = xmlDocument.Parse(rawConfiguration.data(), rawConfiguration.size());
        if (xmlError != XML_SUCCESS)
        {
            auto errorMessage =
                    boost::format("Failed to load configuration from %1%: %2%") %
                    filePath.generic_string() % xmlDocument.ErrorName();
            throw std::runtime_error(errorMessage.str());
        }

        auto xmlConfiguration = xmlDocument.FirstChildElement("configuration");
        if (!xmlConfiguration)
            throw std::runtime_error("Configuration is not defined");

        auto xmlClient = xmlConfiguration->FirstChildElement("client");
        auto rawClientId = xmlClient->Attribute("id");
        if (!rawClientId)
            throw std::runtime_error("ClientID is not defined");
        clientId.assign(rawClientId);

        auto xmlServer = xmlConfiguration->FirstChildElement("server");
        if (!xmlServer)
            throw std::runtime_error("Server parameters are not defined");
        {
            auto rawIp = xmlServer->Attribute("ip");
            if (!rawIp)
                throw std::runtime_error("Server IP is not defined");
            serverIp.assign(rawIp);

            serverPort = static_cast<NetworkPort>(xmlServer->IntAttribute("port", 0));
            if (serverPort == 0)
                throw std::runtime_error("Server port is not defned");
        }

        auto xmlTransmisstion = xmlConfiguration->FirstChildElement("transmission");
        if (!xmlTransmisstion)
            throw std::runtime_error("Transmission parameters are not defined");
        maxTransmittedChunkSizeMB = xmlTransmisstion->IntAttribute("max_chunk_size_mb", 1);

        auto xmlBackupEntries = xmlConfiguration->FirstChildElement("backupEntries");
        if (xmlBackupEntries)
        {
            auto xmlEntry = xmlBackupEntries->FirstChildElement("entry");
            while (xmlEntry)
            {
                auto rawPath = xmlEntry->Attribute("path");
                if (!rawPath)
                    std::runtime_error("Backup entry path is not defined");

                std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
                auto widePath = converter.from_bytes(rawPath);

                boost::filesystem::path path(widePath.data());
                if (!boost::filesystem::exists(path))
                    throw std::runtime_error(std::string("Backup entry doesn't exist: ") + rawPath);
                backupEntries.emplace_front(std::move(path));
                xmlEntry = xmlEntry->NextSiblingElement("entry");
            }
        }
    }
}
