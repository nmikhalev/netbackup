#pragma once

#include <forward_list>
#include <string>

#include <boost/filesystem.hpp>

#include "../Shared/NetworkPort.h"

namespace NetBackup
{
    struct Settings
    {
        std::string clientId;
        std::string serverIp;
        NetworkPort serverPort = -1;
        size_t maxTransmittedChunkSizeMB = -1;
        std::forward_list<boost::filesystem::path> backupEntries;

        Settings(const boost::filesystem::path& filePath);
    };
}

