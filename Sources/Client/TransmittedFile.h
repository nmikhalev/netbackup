#pragma once

#include <memory>

#include "../Shared/ByteArray.h"

namespace NetBackup
{
    class FileInfo;
    class FileReader;

    struct TransmittedFile
    {
        std::unique_ptr<FileInfo> info;
        std::unique_ptr<FileReader> reader;
        ByteArray readBuffer;

        TransmittedFile(std::unique_ptr<FileInfo>&& info) : info(std::move(info))
        {
        }
    };
}
