#include <boost/filesystem.hpp>

#include "../Shared/Logger.h"
#include "../Shared/TcpSocket.h"
#include "Client.h"
#include "FileWriter.h"

namespace NetBackup
{
    Client::Client(const boost::filesystem::path& globalBackupRoot,
                   const SharedTcpSocket& socket) :
        m_socket(socket),
        remoteIp(socket->remoteIp()),
        globalBackupRoot(globalBackupRoot),
        m_connectionStartTimestamp(Time::now())
    {        
    }

    void Client::setId(const std::string& id)
    {
        m_id = id;
        m_backupRoot = globalBackupRoot / m_id;
        Logger::info(boost::format("ID of %1% is '%2%'") % remoteIp % m_id);
    }

    boost::filesystem::path Client::resolveRemotePath(const boost::filesystem::path& remotePath) const
    {
        auto genericRemotePath = remotePath.generic_wstring();
        auto driveDelimiterPosition = genericRemotePath.find(L':');
        if (driveDelimiterPosition != std::string::npos)
            genericRemotePath[driveDelimiterPosition] = boost::filesystem::path::preferred_separator;
        for (auto& symbol : genericRemotePath)
            if (symbol == L'"') symbol = L'_';
        NETBACKUP_PRECONDITION(!m_backupRoot.empty());
        return m_backupRoot / genericRemotePath;
    }

    void Client::createOutputFile(const boost::filesystem::path& filePath)
    {
        if (m_outputFile)
            m_outputFile->close();

        auto fileDirectory = filePath.parent_path();
        if (!boost::filesystem::is_directory(fileDirectory))
            boost::filesystem::create_directories(fileDirectory);

        m_outputFile = std::make_unique<FileWriter>(filePath);
        Logger::info(std::wstring(L"Accepting: ") + filePath.generic_wstring());
    }
}
