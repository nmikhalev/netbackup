#pragma once

#include <string>
#include <memory>

#include <boost/filesystem.hpp>

#include "ForwardDeclarations.h"
#include "Time.hpp"

namespace NetBackup
{
    class Client
    {
    private:
        SharedTcpSocket m_socket;
        std::string remoteIp;
        std::string m_id;

        boost::filesystem::path globalBackupRoot;
        boost::filesystem::path m_backupRoot;

        std::time_t m_connectionStartTimestamp;
        std::unique_ptr<FileWriter> m_outputFile;

    public:
        Client(const boost::filesystem::path& globalBackupRoot,
               const SharedTcpSocket& socket);

    public:
        void setId(const std::string& id);

        const auto& socket() const { return m_socket; }

        const auto& backupRoot() const { return m_backupRoot; }
        boost::filesystem::path resolveRemotePath(const boost::filesystem::path& remotePath) const;

        const auto& connectionStartTimestamp() const { return m_connectionStartTimestamp; }

        void createOutputFile(const boost::filesystem::path& filePath);
        const auto& outputFile() const { return m_outputFile; }
    };
}
