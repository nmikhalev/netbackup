#include <ctime>
#include <forward_list>
#include <thread>

#include <boost/asio.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>

#include "../Shared/FileHash.h"
#include "../Shared/NetworkProtocol/Agreements.h"
#include "../Shared/NetworkProtocol/NetBackup.pb.h"
#include "../Shared/NetworkProtocol/Packet.h"
#include "../Shared/Logger.h"
#include "../Shared/TcpSocket.h"
#include "../Shared/Workflow.h"
#include "Client.h"
#include "Controller.h"
#include "FileWriter.h"
#include "Time.hpp"

namespace NetBackup
{
    using namespace NetworkProtocol;

    Controller::Controller(boost::asio::io_service& ioService) :
        ioService(ioService),
        serverSocket(ioService)
    {
    }

    void Controller::start(const std::shared_ptr<Settings>& settings)
    {
        m_settings = settings;
        Logger::info(std::wstring(L"Backup root: ") + m_settings->backupRoot.generic_wstring());

        using namespace boost::asio::ip;
        tcp::endpoint serverEndpoint(tcp::v4(), settings->listenPort);
        serverSocket.open(serverEndpoint.protocol());
        serverSocket.set_option(tcp::acceptor::reuse_address(true));
        serverSocket.bind(serverEndpoint);
        serverSocket.listen();
        Logger::info(boost::format("Listening port %1%...") % settings->listenPort);

        acceptNextClient();
    }

    void Controller::acceptNextClient()
    {
        auto clientSocket = std::make_shared<TcpSocket>(ioService);
        clientSocket->asyncAccept(
            serverSocket,
            boost::bind(&Controller::onClientAccepted, shared_from_this(),
                        boost::asio::placeholders::error,
                        clientSocket));
    }

    void Controller::onClientAccepted(const boost::system::error_code& error,
                                      SharedTcpSocket clientSocket)
    {
        try
        {
            acceptNextClient();

            if (error)
                throw std::runtime_error(
                    (boost::format("Failed to accept client in socket #%1%: %2%") %
                     clientSocket->id() % error.message()).str());

            Logger::info(std::string("Connected client ") + clientSocket->remoteIp());

            auto client = std::make_shared<Client>(m_settings->backupRoot, clientSocket);
            auto receiveBuffer = makeSharedByteArray(0);
            listenClient(client, receiveBuffer);
        }
        catch (const std::exception& e)
        {
            Logger::error(std::string(NETBACKUP_PRETTY_FUNCTION) + " : " + e.what());
        }
    }

    void Controller::listenClient(const SharedClient& client,
                                  const SharedByteArray& receiveBuffer)
    {
        receiveBuffer->resize(sizeof(PacketSize));
        client->socket()->asyncReceive(
            boost::asio::buffer(receiveBuffer->data(), receiveBuffer->size()),
            boost::bind(&Controller::onPacketSizeReceived, shared_from_this(),
                        boost::asio::placeholders::error,
                        client, receiveBuffer));
    }

    void Controller::onPacketSizeReceived(const boost::system::error_code& error,
                                          SharedClient client,
                                          SharedByteArray receiveBuffer)
    {
        try
        {
            if (error)
                throw std::runtime_error(error.message());

            auto totalSize = *(reinterpret_cast<const Packet::Size*>(receiveBuffer->data()));
            receiveBuffer->resize(totalSize, ByteArray::PreserveContent::Yes);
            client->socket()->asyncReceive(
                boost::asio::buffer(receiveBuffer->data() + sizeof(Packet::Size),
                                    receiveBuffer->size() - sizeof(Packet::Size)),
                boost::bind(&Controller::onPacketReceived, shared_from_this(),
                            boost::asio::placeholders::error,
                            client, receiveBuffer));
        }
        catch (const std::exception& e)
        {
            Logger::error(std::string(NETBACKUP_PRETTY_FUNCTION) + " : " + e.what());
        }
    }

    void Controller::onPacketReceived(const boost::system::error_code& error,
                                      SharedClient client,
                                      SharedByteArray receiveBuffer)
    {
        try
        {
            if (error)
                throw std::runtime_error(error.message());

            auto continueListening = true;
            auto packetType = Packet::typeOf(*receiveBuffer);
            switch (packetType)
            {
            case Packet::kInitialRequest:
                processInitialRequest(client, receiveBuffer);
                break;
            case Packet::kUploadFileRequest:
                processUploadFileRequest(client, receiveBuffer);
                break;
            case Packet::kSavePayloadRequest:
                processSavePayloadRequest(client, receiveBuffer);
                break;
            case Packet::kFinishNotification:
                Logger::info("Transmission is completed");
                removeObsoleteFiles(client);
                continueListening = false;
                break;
            default:
                Logger::error(boost::format("Received unknown packet via socket #%1%") % client->socket()->id());
                continueListening = false;
            }
            if (continueListening)
                listenClient(client, receiveBuffer);
        }
        catch (const std::exception& e)
        {
            Logger::error(std::string(NETBACKUP_PRETTY_FUNCTION) + " : " + e.what());
        }
    }

    void Controller::processInitialRequest(const SharedClient& client,
                                           const SharedByteArray& receiveBuffer)
    {
        InitialRequest initialRequest;
        auto packetPayload = Packet::rawPayloadOf(*receiveBuffer);
        if (!initialRequest.ParseFromArray(packetPayload.first, packetPayload.second))
            throw std::runtime_error("Failed to parse initial request");

        auto& clientId = initialRequest.clientid();
        client->setId(clientId);

        InitialResponse response;
        response.set_protocolversion(NetworkProtocol::kVersion);
        auto serializedResponse = response.SerializeAsString();
        auto packet = Packet::create(Packet::kInitialResponse, serializedResponse);

        BoostErrorCode error;
        client->socket()->send(boost::asio::buffer(packet.data(), packet.size()), error);
        if (error)
            throw ConnectionFailure("Failed to send initial response: ", error.message());
    }

    void Controller::processUploadFileRequest(const SharedClient& client,
                                              const SharedByteArray& receiveBuffer)
    {
        UploadFileRequest uploadRequest;
        auto packetPayload = Packet::rawPayloadOf(*receiveBuffer);
        if (!uploadRequest.ParseFromArray(packetPayload.first, packetPayload.second))
            throw std::runtime_error("Failed to parse request for uploading file");

        UploadFileResponse uploadResponse;
        try
        {
            auto& rawReceivedFilePath = uploadRequest.filepath();
            auto wsRemoteFilePath = fromUtf8(rawReceivedFilePath.data());
            boost::filesystem::path remoteFilePath(wsRemoteFilePath);
            Logger::info(std::wstring(L"Received request to upload ") + wsRemoteFilePath);

            auto localFilePath = client->resolveRemotePath(remoteFilePath);
            auto nativeLocalFilePath = localFilePath.native();
            if (boost::filesystem::exists(localFilePath))
            {
                auto localFileHash = FileHash::computeMD5(nativeLocalFilePath);
                auto& remoteFileHash = uploadRequest.filehash();
                if (std::memcmp(localFileHash.data(), remoteFileHash.data(), localFileHash.size()) == 0)
                {
                    Logger::debug("Hash of the local file is the same");
                    boost::filesystem::last_write_time(localFilePath, Time::now());
                    uploadResponse.set_isconfirmed(false);
                    uploadResponse.set_isuploaded(true);
                }
                else
                {
                    Logger::debug("Hash of the local file is differ");
                    boost::filesystem::remove(localFilePath);
                    uploadResponse.set_isconfirmed(true);
                }
            }
            else uploadResponse.set_isconfirmed(true);

            if (uploadResponse.isconfirmed())
                client->createOutputFile(nativeLocalFilePath);
        }
        catch (const std::exception& e)
        {
            uploadResponse.set_isconfirmed(false);
            uploadResponse.set_rejectioncause(e.what());
        }

        BoostErrorCode error;
        auto serializedResponse = uploadResponse.SerializeAsString();
        auto packet = Packet::create(Packet::kUploadFileResponse, serializedResponse);
        client->socket()->send(boost::asio::buffer(packet.data(), packet.size()), error);
        if (error)
            throw ConnectionFailure(std::string("Failed to send response to upload file: ") + error.message());
    }

    void Controller::processSavePayloadRequest(const SharedClient& client,
                                               const SharedByteArray& receiveBuffer)
    {
        SavePayloadRequest request;
        auto packetPayload = Packet::rawPayloadOf(*receiveBuffer);
        if (!request.ParseFromArray(packetPayload.first, packetPayload.second))
            throw std::runtime_error("Failed to parse request for saving payload");

        SavePayloadResponse response;
        try
        {
            auto& filePayload = request.payload();
            auto& file = client->outputFile();
            NETBACKUP_PRECONDITION(file);
            file->write(filePayload.data(), filePayload.size());
            response.set_ispayloadsaved(true);
        }
        catch (const std::exception& e)
        {
            response.set_ispayloadsaved(false);
            response.set_errordescription(e.what());
        }

        BoostErrorCode error;
        auto serializedResponse = response.SerializeAsString();
        auto packet = Packet::create(Packet::kSavePayloadResponse, serializedResponse);
        client->socket()->send(boost::asio::buffer(packet.data(), packet.size()), error);
        if (error)
            throw ConnectionFailure(std::string("Failed to send response to upload file: ") + error.message());
    }

    void Controller::removeObsoleteFiles(const SharedClient& client)
    {
        static const auto kMaxSchedulesFiles = 10000;
        auto backupStartTime = client->connectionStartTimestamp();
        auto& backupRoot = client->backupRoot();
        while (true)
        {
            std::list<boost::filesystem::path> obsoleteFiles;
            boost::filesystem::recursive_directory_iterator directoryIterator(backupRoot);
            boost::filesystem::recursive_directory_iterator directoryIteratorEnd;
            while (directoryIterator != directoryIteratorEnd)
            {
                if (boost::filesystem::is_regular_file(*directoryIterator))
                {
                    boost::filesystem::path filePath(*directoryIterator);
                    try
                    {
                        auto modificationTime = boost::filesystem::last_write_time(filePath);
                        if (modificationTime < backupStartTime)
                        {
                            obsoleteFiles.emplace_back(filePath);
                            if (obsoleteFiles.size() == kMaxSchedulesFiles)
                                break;
                        }
                    }
                    catch (const std::exception& e)
                    {
                        Logger::error(boost::wformat(L"Failed to process possible obsolete file %1%: %2%") %
                                      filePath.generic_wstring() % e.what());
                    }
                }
                ++directoryIterator;
            }

            if (!obsoleteFiles.empty())
            {
                auto scheduledFilesNumber = obsoleteFiles.size();
                Logger::info(boost::format("Scheduled %1% obsolete files for removing") % scheduledFilesNumber);
                for (auto& obsoleteFilePath : obsoleteFiles)
                {
                    try
                    {
                        boost::filesystem::remove(obsoleteFilePath);
                    }
                    catch (const std::exception& e)
                    {
                        Logger::error(std::string("Failed to remove file ") + e.what());
                    }
                }
                if (scheduledFilesNumber < kMaxSchedulesFiles)
                    break;
            }
            else break;
        }
    }
}
