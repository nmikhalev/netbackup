#pragma once

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <memory>
#include <string>

#include "../Shared/ByteArray.h"
#include "../Shared/NetworkPort.h"
#include "Client.h"
#include "Settings.h"

namespace NetBackup
{
    class FileEnumerator;
    class FileWriter;
    class TcpSocket;

    class Controller : public std::enable_shared_from_this<Controller>
    {
    public:
        using BoostErrorCode = boost::system::error_code;
        using SharedClient = std::shared_ptr<Client>;
        using SharedTcpSocket = std::shared_ptr<TcpSocket>;
        using SharedDeadlineTimer = std::shared_ptr<boost::asio::deadline_timer>;
        using SharedFileWriter = std::shared_ptr<FileWriter>;

    public:
        boost::asio::io_service& ioService;
        std::shared_ptr<Settings> m_settings;
        boost::asio::ip::tcp::acceptor serverSocket;

    public:
        Controller(boost::asio::io_service& ioService);

    public:
        void start(const std::shared_ptr<Settings>& settings);

    private:
        void onClientAccepted(
                const boost::system::error_code& error,
                SharedTcpSocket clientSocket);
        void onPacketReceived(
                const boost::system::error_code& error,
                SharedClient client,
                SharedByteArray receiveBuffer);
        void onPacketSizeReceived(
                const boost::system::error_code& error,
                SharedClient client,
                SharedByteArray receiveBuffer);

    private:
        void acceptNextClient();
        void listenClient(
                const SharedClient& client,
                const SharedByteArray& receiveBuffer);
        void removeObsoleteFiles(
                const SharedClient& client);
        void processInitialRequest(
                const SharedClient& client,
                const SharedByteArray& receiveBuffer);
        void processSavePayloadRequest(
                const SharedClient& client,
                const SharedByteArray& receiveBuffer);
        void processUploadFileRequest(
                const SharedClient& client,
                const SharedByteArray& receiveBuffer);
    };
}
