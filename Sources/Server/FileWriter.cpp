#include <stdexcept>

#include "../Shared/PlatformInfo.h"
#ifndef NETBACKUP_LINUX
#ifndef NETBACKUP_WINDOWS
#error "Platform is not supported"
#endif
#endif

#include <fcntl.h>
#ifdef NETBACKUP_LINUX
#include <sys/types.h>
#include <sys/stat.h>
#endif // NETBACKUP_LINUX
#ifdef NETBACKUP_WINDOWS
#include <io.h>
#endif // NETBACKUP_WINDOWS

#include "FileWriter.h"

namespace NetBackup
{
    FileWriter::FileWriter(const boost::filesystem::path& filePath) : m_filePath(filePath)
    {
#ifdef NETBACKUP_LINUX
        fileHandle = ::open(filePath.native().data(), O_CREAT | O_WRONLY | O_TRUNC, S_IREAD | S_IWRITE);
#endif
#ifdef NETBACKUP_WINDOWS
        fileHandle = ::_wopen(filePath.native().data(), _O_CREAT | _O_BINARY | _O_WRONLY | _O_TRUNC, _S_IREAD | _S_IWRITE);
#endif
        if (fileHandle <= 0)
            throw std::runtime_error(std::string("Failed to open file ") + m_filePath.generic_string());
    }

    FileWriter::~FileWriter()
    {
        try { close(); } catch (...) { }
    }

    void FileWriter::close()
    {
        if (!isClosed)
        {
#ifdef NETBACKUP_LINUX
                ::close(fileHandle);
#endif
#ifdef NETBACKUP_WINDOWS
                ::_close(fileHandle);
#endif
            isClosed = true;
        }
    }

    void FileWriter::write(const char* buffer, unsigned int bufferSize)
    {
#ifdef NETBACKUP_LINUX
        auto bytesWritten = ::write(fileHandle, buffer, bufferSize);
#endif
#ifdef NETBACKUP_WINDOWS
        auto bytesWritten = ::_write(fileHandle, buffer, bufferSize);
#endif
        if (bytesWritten != bufferSize)
            throw std::runtime_error(std::string("Failed to write to ") + m_filePath.generic_string());
    }
}
