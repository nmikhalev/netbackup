#pragma once

#include <string>
#include <boost/filesystem/fstream.hpp>

namespace NetBackup
{
    class FileWriter
    {
    private:
        boost::filesystem::path m_filePath;
        int fileHandle = -1;
        bool isClosed = false;

    public:
        FileWriter(const boost::filesystem::path& filePath);
        FileWriter(const FileWriter&) = delete;
        FileWriter& operator=(const FileWriter&) = delete;
        virtual ~FileWriter();

    public:
        void write(const char* buffer, unsigned int bufferSize);
        void close();
    };
}
