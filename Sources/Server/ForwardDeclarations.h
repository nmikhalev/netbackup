#pragma once

#include <memory>

namespace NetBackup
{
    class TcpSocket;
    using SharedTcpSocket = std::shared_ptr<TcpSocket>;

    class FileWriter;
}
