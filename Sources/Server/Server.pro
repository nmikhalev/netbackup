TARGET = NBServer
TEMPLATE = app
CONFIG += console c++14
CONFIG -= qt

CONFIG(debug, debug|release) {
    DESTDIR = debug
} else {
    DESTDIR = release
}

THIRD_PARTY_COMPONENTS_PATH = $$_PRO_FILE_PWD_/../3rd_party

# Add Protocol Buffer
unix {
    LIBS += -lprotobuf
}
win32 {
    CONFIG(debug, debug|release) {
        PROTOBUF_LIB = libprotobufd.lib
    } else {
        PROTOBUF_LIB = libprotobuf.lib
    }
    INCLUDEPATH += $$THIRD_PARTY_COMPONENTS_PATH/protobuf-3.0.0-vs2015/include
    LIBS += -L$$THIRD_PARTY_COMPONENTS_PATH/protobuf-3.0.0-vs2015/lib $$PROTOBUF_LIB
}

# Add Boost
unix {
    LIBS += -lboost_system -lboost_filesystem
}
win32 {
    INCLUDEPATH += $$THIRD_PARTY_COMPONENTS_PATH/boost-1.62.0/include
    LIBS += -L$$THIRD_PARTY_COMPONENTS_PATH/boost-1.62.0/lib/x86
    DEFINES += _WIN32_WINNT=0x0501
}

# Add Shared component
unix {
    LIBS += -L../Shared -lNetBackupShared
}
win32 {
    LIBS += -L../Shared/$$DESTDIR -lNetBackupShared
}
QMAKE_EXTRA_TARGETS += NetBackupShared
NetBackupShared.depends = FORCE
PRE_TARGETDEPS += NetBackupShared
DEPENDPATH += ../Shared

SOURCES += \
    Client.cpp \
    Controller.cpp \
    Settings.cpp \
    main.cpp \
    FileWriter.cpp

HEADERS += \
    Client.h \
    Controller.h \
    Settings.h \
    ForwardDeclarations.h \
    FileWriter.h \
    Time.hpp

# Add TinyXML
SOURCES += ../3rd_party/tinyxml2/tinyxml2.cpp
INCLUDEPATH += ../3rd_party/tinyxml2
