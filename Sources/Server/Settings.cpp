#include <boost/format.hpp>
#include <tinyxml2.h>

#include "../Shared/Logger.h"
#include "../Shared/FileReader.h"
#include "../Shared/StringConverter.h"
#include "Settings.h"

namespace NetBackup
{
    Settings::Settings(const boost::filesystem::path& filePath)
    {
        using namespace tinyxml2;
        auto rawConfiguration = FileReader::readAll(filePath);

        XMLDocument xmlDocument;
        auto xmlError = xmlDocument.Parse(rawConfiguration.data(), rawConfiguration.size());
        if (xmlError != XML_SUCCESS)
        {
            auto errorMessage =
                    boost::format("Failed to load configuration from %1%: %2%") %
                    filePath.generic_string() % xmlDocument.ErrorName();
            throw std::runtime_error(errorMessage.str());
        }

        auto xmlConfiguration = xmlDocument.FirstChildElement("configuration");
        if (!xmlConfiguration)
            throw std::runtime_error("Configuration is not defined");

        auto xmlNetwork = xmlConfiguration->FirstChildElement("network");
        listenPort = static_cast<NetworkPort>(xmlNetwork->IntAttribute("port", 0));
        if (listenPort == 0)
            throw std::runtime_error("Listen port is not defined");

        auto xmlFileSystem = xmlConfiguration->FirstChildElement("filesystem");
        if (!xmlFileSystem)
            throw std::runtime_error("FS parameters are not defined");
        auto rawBackupRoot = xmlFileSystem->Attribute("backupRoot");
        if (!rawBackupRoot)
            std::runtime_error("Backup root path is not defined");
        backupRoot = boost::filesystem::path(rawBackupRoot);
    }
}
