#pragma once

#include <string>

#include <boost/filesystem.hpp>

#include "../Shared/NetworkPort.h"

namespace NetBackup
{
    struct Settings
    {
        NetworkPort listenPort = -1;
        boost::filesystem::path backupRoot;

        Settings(const boost::filesystem::path& filePath);
    };
}

