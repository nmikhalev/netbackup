#pragma once

#include <ctime>

namespace NetBackup
{
    class Time
    {
    public:
        static std::time_t now()
        {
            std::time_t currentTimestamp;
            ::time(&currentTimestamp);
            return currentTimestamp;
        }
    };
}
