#include <boost/asio/io_service.hpp>

#include "../Shared/Logger.h"
#include "Controller.h"
#include "Settings.h"

using namespace NetBackup;

int main(int /*argc*/, char* /*argv*/[])
{
    Logger::createInstance(Logger::Level::Debug);
    Logger::Deleter loggerDeleter;

    try
    {
        auto settingsFilePath = boost::filesystem::current_path() / "server.xml";
        auto settings = std::make_shared<Settings>(settingsFilePath);

        boost::asio::io_service ioService;
        auto controller = std::make_shared<Controller>(ioService);
        controller->start(settings);

        ioService.run();
        return 0;
    }
    catch (const std::exception& e)
    {
        Logger::error(e.what());
        return 1;
    }
}
