#pragma once

#include <memory>
#include <vector>

namespace NetBackup
{
    class ByteArray
    {
    public:
        enum class PreserveContent { Yes, No };

    private:
        std::unique_ptr<char[]> storage;
        size_t reservedSize = 0;
        size_t actualSize = 0;

    public:
        ByteArray() noexcept
        {
        }

        ByteArray(size_t size) : reservedSize(size), actualSize(size)
        {
            if (size > 0)
                storage.reset(new char[size]);
        }

        ByteArray(ByteArray&& other) noexcept :
            storage(std::move(other.storage)),
            reservedSize(other.reservedSize),
            actualSize(other.actualSize)
        {
            other.reservedSize = 0;
            other.actualSize = 0;
        }

        ByteArray& operator=(ByteArray&& other) noexcept
        {
            if (this != &other)
            {
                storage = std::move(other.storage);
                reservedSize = other.reservedSize;
                actualSize = other.actualSize;

                other.reservedSize = 0;
                other.actualSize = 0;
            }
            return *this;
        }

    public:
        char* data() noexcept
        {
            return storage.get();
        }

        const char* data() const noexcept
        {
            return storage.get();
        }

        size_t size() const noexcept
        {
            return actualSize;
        }

        void resize(size_t size, PreserveContent preserveContent = PreserveContent::No)
        {
            if (reservedSize < size)
            {
                if (preserveContent == PreserveContent::Yes)
                {
                    auto newStorage = std::make_unique<char[]>(size);
                    std::copy(storage.get(), storage.get() + actualSize, newStorage.get());
                    storage = std::move(newStorage);
                }
                else
                {
                    storage.reset(new char[size]);
                }
                reservedSize = size;
            }
            actualSize = size;
        }

        bool empty() const noexcept
        {
            return (actualSize == 0);
        }

        char& operator[](size_t index)
        {
            return storage[index];
        }
    };

    using SharedByteArray = std::shared_ptr<ByteArray>;

    static inline SharedByteArray makeSharedByteArray(size_t size)
    {
        return std::make_shared<ByteArray>(size);
    }
}
