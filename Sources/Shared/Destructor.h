#pragma once

#include <functional>

namespace NetBackup
{
    class Destructor
    {
    private:
        using HandlerType = void();

    private:
        bool isCancelled = false;
        std::function<HandlerType> destructionHandler;

    public:
        Destructor() : isCancelled(true)
        {
        }

        Destructor(std::function<HandlerType>&& destructionHandler) : destructionHandler(destructionHandler)
        {
        }

    public:
        void bind(std::function<HandlerType>&& destructionHandler)
        {
            this->destructionHandler = std::move(destructionHandler);
            isCancelled = false;
        }

        void cancel() noexcept
        {
            isCancelled = true;
        }

    public:
        virtual ~Destructor()
        {
            if (!isCancelled)
                destructionHandler();
        }
    };
}
