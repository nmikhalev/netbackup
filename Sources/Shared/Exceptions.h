#pragma once

#include <stdexcept>

namespace NetBackup
{
    class ConnectionFailure : public std::runtime_error
    {
    public:
        ConnectionFailure(const std::string& description) : std::runtime_error(description)
        {
        }

        ConnectionFailure(const std::string& description, const std::string& cause) : std::runtime_error(description + cause)
        {
        }
    };
}
