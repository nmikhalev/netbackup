#include <vector>

#include "FileHash.h"
#include "FileReader.h"
#include "Md5.h"

namespace NetBackup
{
    ByteArray FileHash::computeMD5(const boost::filesystem::path& fileName)
    {
        FileReader file(fileName);
        ByteArray buffer(1 * 1024 * 1024);
        MD5Context context;
        MD5Init(&context);
        while (true)
        {
            auto bytesRead = file.readSome(buffer.data(), buffer.size());
            if (bytesRead > 0)
                MD5Update(&context, (unsigned char*)buffer.data(), bytesRead);
            else
                break;
        }

        buffer.resize(kMD5HashSize);
        MD5Final(&context, reinterpret_cast<unsigned char*>(buffer.data()));
        return buffer;
    }
}
