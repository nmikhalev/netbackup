#pragma once

#include <boost/filesystem.hpp>
#include "ByteArray.h"

namespace NetBackup
{
    class FileHash
    {
    public:
        static ByteArray computeMD5(const boost::filesystem::path& fileName);
    };
}
