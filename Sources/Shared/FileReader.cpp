#include <stdexcept>

#include <boost/filesystem.hpp>

#include "../Shared/PlatformInfo.h"
#ifndef NETBACKUP_LINUX
#ifndef NETBACKUP_WINDOWS
#error "Platform is not supported"
#endif
#endif

#include <fcntl.h>
#ifdef NETBACKUP_LINUX
#include <sys/types.h>
#include <sys/stat.h>
#endif // NETBACKUP_LINUX
#ifdef NETBACKUP_WINDOWS
#include <io.h>
#endif // NETBACKUP_WINDOWS

#include "FileReader.h"

namespace NetBackup
{
    FileReader::FileReader(const boost::filesystem::path& filePath) : m_filePath(filePath)
    {
#ifdef NETBACKUP_LINUX
        fileHandle = ::open(filePath.native().data(), O_RDONLY);
#endif
#ifdef NETBACKUP_WINDOWS
        fileHandle = ::_wopen(filePath.native().data(), _O_BINARY | _O_RDONLY | _O_SEQUENTIAL);
#endif
        if (fileHandle <= 0)
            throw std::runtime_error(std::string("Failed to open file ") + m_filePath.generic_string());
    }
    
    FileReader::~FileReader()
    {
        if (fileHandle > 0)
        {
#ifdef NETBACKUP_LINUX
            ::close(fileHandle);
#endif
#ifdef NETBACKUP_WINDOWS
            ::_close(fileHandle);
#endif
        }
    }
    
    int FileReader::readSome(char* outputBuffer, unsigned int outputBufferSize)
    {
        auto bytesRead =
#ifdef NETBACKUP_LINUX
                ::read(fileHandle, outputBuffer, outputBufferSize);
#endif
#ifdef NETBACKUP_WINDOWS
                ::_read(fileHandle, outputBuffer, outputBufferSize);
#endif
        if (bytesRead < 0)
            throw std::runtime_error(std::string("Failed to read file ") + m_filePath.generic_string());
        return bytesRead;
    }

    ByteArray FileReader::readAll(const boost::filesystem::path& filePath)
    {
        ByteArray fileData;
        auto fileSize = boost::filesystem::file_size(filePath);
        if (fileSize > 0)
        {
            fileData.resize(fileSize);
            FileReader reader(filePath);
            auto bytesRead = reader.readSome(fileData.data(), fileSize);
            fileData.resize(bytesRead, ByteArray::PreserveContent::Yes);
        }
        return fileData;
    }
}
