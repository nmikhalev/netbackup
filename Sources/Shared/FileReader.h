#pragma once

#include <boost/filesystem.hpp>
#include "../Shared/ByteArray.h"

namespace NetBackup
{    
    class FileReader
    {
    private:
        boost::filesystem::path m_filePath;
        int fileHandle = -1;

    public:
        FileReader() = delete;
        FileReader(const boost::filesystem::path& filePath);
        FileReader(const FileReader&) = delete;
        FileReader& operator=(const FileReader&) = delete;
        virtual ~FileReader();

    public:
        int readSome(char* outputBuffer, unsigned int outputBufferSize);
        static ByteArray readAll(const boost::filesystem::path& filePath);
    };
}
