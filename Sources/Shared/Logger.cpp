#include <iostream>
#include <stdexcept>
#include <thread>

#include "PlatformInfo.h"
#ifdef NETBACKUP_WINDOWS
#include <fcntl.h>
#include <io.h>
#endif // NETBACKUP_WINDOWS

#include <boost/date_time.hpp>
#include <boost/format.hpp>

#include "Logger.h"
#include "ProcessInfo.h"

namespace NetBackup
{
    Logger::Logger(Level level) : loggingLevel(level)
    {
#ifdef NETBACKUP_WINDOWS
        ::_setmode(::_fileno(stdout), _O_U16TEXT);
#endif // NETBACKUP_WINDOWS
    }

    void Logger::setLevel(Logger::Level level)
    {
        loggingLevel = level;
    }

    Logger::Level Logger::getLevel() const
    {
        return loggingLevel;
    }

    bool Logger::isDebugMessagesPrinted()
    {
        try
        {
            if (Logger::isInstanceCreated())
            {
                auto& thisObject = Logger::getInstance();
                return (Level::Debug <= thisObject->loggingLevel);
            }
        }
        catch (...) { }
        return false;
    }

    void Logger::debug(const std::wstring& messageText)
    {
        write(Level::Debug, messageText);
    }

    void Logger::info(const std::wstring& messageText)
    {
        write(Level::Info, messageText);
    }

    void Logger::warning(const std::wstring& messageText)
    {
        write(Level::Warning, messageText);
    }

    void Logger::error(const std::wstring& messageText)
    {
        write(Level::Error, messageText);
    }

    void Logger::error(const std::wstring& shortDescription, const std::wstring& cause)
    {
        auto messageText = shortDescription + cause;
        write(Level::Error, messageText);
    }

    void Logger::write(Level messageLevel, const std::wstring& messageText)
    {
        try
        {
            if (Logger::isInstanceCreated())
            {
                auto& thisObject = Logger::getInstance();
                if (messageLevel <= thisObject->loggingLevel)
                {
                    auto message = formatMessage(messageLevel, messageText);
                    std::wcout << message << std::endl;
                }
            }
        }
        catch (...)
        {
        }
    }

    std::wstring Logger::formatMessage(Level messageLevel, const std::wstring& messageText)
    {
        auto currentDateTime = getCurrentDateTime();
        auto processId = Process::getPid();
        auto stringMessageLevel = toString(messageLevel);
        return (currentDateTime + L" [" + std::to_wstring(processId) + L"] [" + stringMessageLevel + L"] " + messageText);
    }

    std::wstring Logger::getCurrentDateTime()
    {
        std::wstringstream stringStream;
        stringStream << boost::posix_time::microsec_clock::local_time();
        return stringStream.str();
    }

    const wchar_t* Logger::toString(Level& level) noexcept
    {
        switch (level)
        {
            case Level::Info:
                return L"Info";
            case Level::Warning:
                return L"Warning";
            case Level::Error:
                return L"Error";
            case Level::Debug:
                return L"Debug";
            default:
                return L"???";
        };
    }
}
