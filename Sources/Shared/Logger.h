#pragma once

#include <stdexcept>
#include <boost/format.hpp>
#include <fstream>
#include <mutex>

#include "Singleton.h"
#include "StringConverter.h"
#include "Workflow.h"

#define NETBACKUP_PRECONDITION(condition) \
    { \
        if (!(condition)) \
        { \
            auto errorText = std::string("Precondition failed in `") + NETBACKUP_PRETTY_FUNCTION + "`: " + #condition; \
            NetBackup::Logger::error(errorText); \
            throw std::logic_error(errorText); \
        } \
    }

namespace NetBackup
{
    class Logger : public Singleton<Logger>
    {
    public:
        enum Level { Error, Warning, Info, Debug };
        enum class DuplicateToStdout { Yes, No };

    private:
        Level loggingLevel = Logger::Level::Info;

    public:
        Logger(Level level = Level::Debug);

    public:
        void setLevel(Level level);
        Level getLevel() const;
        static bool isDebugMessagesPrinted();
        static void debug(const std::wstring& messageText);
        static void info(const std::wstring& messageText);
        static void warning(const std::wstring& messageText);
        static void error(const std::wstring& messageText);
        static void error(const std::wstring& shortDescription, const std::wstring& cause);

        static void debug(const std::string& messageText)
        {
            debug(toWString(messageText));
        }

        static void info(const std::string& messageText)
        {
            info(toWString(messageText));
        }

        static void warning(const std::string& messageText)
        {
            warning(toWString(messageText));
        }

        static void error(const std::string& messageText)
        {
            error(toWString(messageText));
        }

        static void error(const std::string& shortDescription, const std::string& cause)
        {
            error(toWString(shortDescription), toWString(cause));
        }

        template<typename T>
        static void debug(const boost::basic_format<T>& messageText)
        {
            debug(messageText.str());
        }

        template<typename T>
        static void info(const boost::basic_format<T>& messageText)
        {
            info(messageText.str());
        }

        template<typename T>
        static void warning(const boost::basic_format<T>& messageText)
        {
            warning(messageText.str());
        }

        template<typename T>
        static void error(const boost::basic_format<T>& messageText)
        {
            error(messageText.str());
        }

    private:
        static void write(Level messageLevel, const std::wstring& messageText);
        static std::wstring formatMessage(Level messageLevel, const std::wstring& messageText);
        static std::wstring getCurrentDateTime();
        static const wchar_t* toString(Level& level) noexcept;
    };
}
