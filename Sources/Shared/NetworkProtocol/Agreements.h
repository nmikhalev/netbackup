#pragma once

namespace NetBackup
{
    namespace NetworkProtocol
    {
        uint32_t kVersion = 1;

        using PacketSize = uint32_t;
    }
}
