#include <stdexcept>

#include "Packet.h"

namespace NetBackup
{
    namespace NetworkProtocol
    {
        ByteArray Packet::create(Type type, const std::string& payload)
        {
            auto totalSize = sizeof(Header) + payload.size();
            ByteArray packet(totalSize);
            auto packetHeader = reinterpret_cast<Header*>(packet.data());
            packetHeader->totalSize = totalSize;
            packetHeader->type = type;
            std::copy(payload.data(),
                      payload.data() + payload.size(),
                      packet.data() + sizeof(*packetHeader));
            return packet;
        }

        Packet::Type Packet::typeOf(const ByteArray& packet)
        {
            return headerOf(packet)->type;
        }

        std::string Packet::payloadOf(const ByteArray& packet)
        {
            auto rawPayload = rawPayloadOf(packet);
            return std::string(rawPayload.first, rawPayload.second);
        }

        std::pair<const char*, Packet::Size> Packet::rawPayloadOf(const ByteArray& packet)
        {
            auto header = headerOf(packet);
            auto payloadSize = header->totalSize - sizeof(Header);
            auto payloadBegin = packet.data() + sizeof(Header);
            return std::make_pair(payloadBegin, payloadSize);
        }

        const Packet::Header* Packet::headerOf(const ByteArray& packet)
        {
            if (packet.size() < sizeof(Packet::Header))
                throw std::runtime_error("Incomplete packet");
            return reinterpret_cast<const Packet::Header*>(packet.data());
        }        
    }
}

