#pragma once

#include <string>
#include <utility>
#include <vector>

#include "../ByteArray.h"
#include "../PackedStruct.h"

namespace NetBackup
{
    namespace NetworkProtocol
    {
        class Packet
        {
        public:
            using Size = uint32_t;

            enum Type : uint16_t
            {
                kInitialRequest = 1,
                kInitialResponse = 2,

                kUploadFileRequest = 3,
                kUploadFileResponse = 4,

                kSavePayloadRequest = 5,
                kSavePayloadResponse = 6,

                kFinishNotification = 7
            };

        private:
            NETBACKUP_PACKED_STRUCT_BEGIN(Header)
            {
                Size totalSize;
                Type type;
            }
            NETBACKUP_PACKED_STRUCT_END;

        public:
            static ByteArray create(Type type, const std::string& payload);

            template<typename... Arguments>
            static SharedByteArray createShared(Arguments&&... arguments)
            {
                auto packet = create(std::forward<Arguments>(arguments)...);
                return std::make_shared<ByteArray>(std::move(packet));
            }

            static Type typeOf(const ByteArray& packet);
            static std::string payloadOf(const ByteArray& packet);
            static std::pair<const char*, Packet::Size> rawPayloadOf(const ByteArray& packet);

        private:
            static const Header* headerOf(const ByteArray& packet);
        };
    }
}
