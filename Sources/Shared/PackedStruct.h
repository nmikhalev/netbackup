#pragma once

#ifdef _MSC_VER
    #define NETBACKUP_PACKED_STRUCT_BEGIN( name ) \
        __pragma( pack(push, 1) ) struct name
    #define NETBACKUP_PACKED_STRUCT_END \
        __pragma( pack(pop) )

#elif defined(__GNUC__)
    #define NETBACKUP_PACKED_STRUCT_BEGIN( name ) \
        struct __attribute__((__packed__)) name
    #define NETBACKUP_PACKED_STRUCT_END

#endif
