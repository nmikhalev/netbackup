#pragma once

#ifdef _WIN32
#define NETBACKUP_WINDOWS
#elif __linux__
#define NETBACKUP_LINUX
#else
#error "Unknown operating system"
#endif

namespace NetBackup
{
    class PlatformInfo
    {
    public:
        static bool isLinux()
        {
#ifdef NETBACKUP_LINUX
            return true;
#else
            return false;
#endif
        }

        static bool isWindows()
        {
#ifdef NETBACKUP_WINDOWS
            return true;
#else
            return false;
#endif
        }
    };
}
