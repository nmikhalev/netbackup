#include <string>

#include "PlatformInfo.h"
#include "ProcessInfo.h"

#ifdef NETBACKUP_LINUX
#include <unistd.h>
#endif

#ifdef NETBACKUP_WINDOWS
#include <Windows.h>
#endif

namespace NetBackup
{
    int Process::getPid()
    {
#ifdef NETBACKUP_LINUX
        return static_cast<int>(::getpid());
#endif

#ifdef NETBACKUP_WINDOWS
        return ::GetCurrentProcessId();
#endif
    }
}
