TARGET = NetBackupShared
TEMPLATE = lib
CONFIG -= qt
CONFIG += staticlib c++14

THIRD_PARTY_COMPONENTS_PATH = $$_PRO_FILE_PWD_/../3rd_party

# Add Protocol Buffer
unix {
    LIBS += -lprotobuf
}
win32 {
    CONFIG(debug, debug|release) {
        PROTOBUF_LIB = libprotobufd.lib
    } else {
        PROTOBUF_LIB = libprotobuf.lib
    }
    INCLUDEPATH += $$THIRD_PARTY_COMPONENTS_PATH/protobuf-3.0.0-vs2015/include
    LIBS += -L$$THIRD_PARTY_COMPONENTS_PATH/protobuf-3.0.0-vs2015/lib $$PROTOBUF_LIB
}

# Add Boost
unix {
    LIBS += -lboost_system -lboost_filesystem
}
win32 {
    INCLUDEPATH += $$THIRD_PARTY_COMPONENTS_PATH/boost-1.62.0/include
    LIBS += -L$$THIRD_PARTY_COMPONENTS_PATH/boost-1.62.0/lib/x86
    DEFINES += _WIN32_WINNT=0x0501
}

SOURCES += \
    FileHash.cpp \
    FileReader.cpp \
    Logger.cpp \
    Md5.cpp \
    NetworkProtocol/NetBackup.pb.cc \
    NetworkProtocol/Packet.cpp \
    ProcessInfo.cpp \
    TcpSocket.cpp
    
HEADERS += \
    ByteArray.h \
    Destructor.h \
    Exceptions.h \
    FileHash.h \
    FileReader.h \
    Logger.h \
    NetworkPort.h \
    NetworkProtocol.h \
    NetworkProtocol/Agreements.h \
    NetworkProtocol/NetBackup.pb.h \
    NetworkProtocol/Packet.h \
    Md5.h \
    PackedStruct.h \
    PlatformInfo.h \
    Singleton.h \
    TcpSocket.h \
    Workflow.h \
    StringConverter.h \
    ProcessInfo.h
    
