#pragma once

#include <memory>
#include <string>
#include <stdexcept>

namespace NetBackup
{
    template<typename T> 
    class Singleton
    {       
    public:
        class Deleter
        {
        public:
            ~Deleter()
            {
                T::deleteInstance();
            }
        };

    private:
        static std::unique_ptr<T> self;
        
    public:
        Singleton() {}
        virtual ~Singleton() {}
        
    public:
        template<typename... Arguments>
        static const std::unique_ptr<T>& createInstance(Arguments... arguments)
        {
            if (self) throw std::runtime_error("Singleton has been already called");
            return (self = std::unique_ptr<T>(new T(std::forward<Arguments>(arguments)...)));
        }

        static bool isInstanceCreated()
        {
            return (self != nullptr);
        }
        
        static const std::unique_ptr<T>& getInstance()
        {
            if (!self) throw std::runtime_error("Singleton has not been created yet");
            return self;
        }
        
        static void deleteInstance()
        {
            self.reset();
        }
    };

    template<class T> std::unique_ptr<T> Singleton<T>::self(nullptr);
}
