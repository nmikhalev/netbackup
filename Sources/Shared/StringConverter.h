#pragma once

#include <codecvt>
#include <locale>
#include <string>

namespace NetBackup
{
    static inline std::string toString(const std::wstring& wideString)
    {
        return std::string(wideString.cbegin(), wideString.cend());
    }

    static inline std::wstring toWString(const std::string& asciiString)
    {
        return std::wstring(asciiString.cbegin(), asciiString.cend());
    }

    static inline std::wstring reinterpretAsWString(const std::string& buffer)
    {
        auto rawData = reinterpret_cast<const wchar_t*>(buffer.data());
        auto rawDataSize = buffer.size() / sizeof(wchar_t);
        return std::wstring(rawData, rawDataSize);
    }

    static inline std::string toUtf8(const std::wstring& filePath)
    {
        std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
        return converter.to_bytes(filePath);
    }

    static inline std::wstring fromUtf8(const char* serializedFilePath)
    {
        std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
        return converter.from_bytes(serializedFilePath);
    }
}
