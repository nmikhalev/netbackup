#include "Logger.h"
#include "TcpSocket.h"

namespace NetBackup
{
    std::atomic<size_t> TcpSocket::totalSockets(0);
    std::atomic<TcpSocketId> TcpSocket::socketCounter(0);

    TcpSocket::TcpSocket(boost::asio::io_service& ioService) :
        m_id(++socketCounter),
        asioSocket(ioService)
    {
        if (Logger::isDebugMessagesPrinted())
            Logger::debug(boost::format("Constructed TCP-socket #%1% (total: %2%)") %
                          m_id % (++totalSockets));
    }

    TcpSocket::~TcpSocket()
    {
        try
        {
            closeWithoutExceptions();
            if (Logger::isDebugMessagesPrinted())
                Logger::debug((boost::format("Destructed socket #%1% (total: %2%)") %
                               m_id % (--totalSockets)).str());
        }
        catch (...) { }
    }

    void TcpSocket::closeWithoutExceptions()
    {
        try
        {
            if (!isClosed)
            {
                boost::system::error_code errorCode;
                asioSocket.shutdown(boost::asio::ip::tcp::socket::shutdown_both, errorCode);
                asioSocket.close();
                isClosed = true;
            }
        }
        catch (...) { }
    }


    std::string TcpSocket::remoteIp() const
    {
        return asioSocket.remote_endpoint().address().to_string();
    }
}
