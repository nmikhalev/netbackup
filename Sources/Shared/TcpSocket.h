#pragma once

#include <atomic>
#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <memory>

#include "Exceptions.h"

namespace NetBackup
{
    using TcpSocketId = int64_t;

    class TcpSocket : public std::enable_shared_from_this<TcpSocket>
    {
    private:
        static std::atomic<size_t> totalSockets;
        static std::atomic<TcpSocketId> socketCounter;

    private:
        TcpSocketId m_id;
        bool isClosed = false;
        boost::asio::ip::tcp::socket asioSocket;

    public:
        TcpSocket(boost::asio::io_service& ioService);
        ~TcpSocket();

    public:
        TcpSocketId id() const noexcept
        {
            return m_id;
        }

        void closeWithoutExceptions();
        std::string remoteIp() const;

        template<typename Buffer>
        void send(Buffer&& payload, boost::system::error_code& error)
        {
            boost::asio::write(asioSocket, payload, error);
        }

        template<typename Buffer>
        void receive(Buffer&& payload, boost::system::error_code& error)
        {
            boost::asio::read(asioSocket, payload, error);
        }

        template<typename CompletionHandler>
        void asyncAccept(boost::asio::ip::tcp::acceptor& serverSocket, CompletionHandler&& completionHandler)
        {
            serverSocket.async_accept(this->asioSocket, completionHandler);
        }

        template<typename CompletionHandler>
        void asyncConnect(const boost::asio::ip::tcp::endpoint& remoteEndpoint, CompletionHandler&& completionHandler)
        {
            asioSocket.async_connect(remoteEndpoint, completionHandler);
        }

        template<typename Buffer, typename CompletionHandler>
        void asyncSend(Buffer&& payload, CompletionHandler&& handler)
        {
            boost::asio::async_write(asioSocket, payload, handler);
        }

        template<typename Buffer, typename CompletionHandler>
        void asyncReceiveSome(Buffer&& receiveBuffer, CompletionHandler&& handler)
        {
            asioSocket.async_read_some(receiveBuffer, handler);
        }

        template<typename Buffer, typename CompletionHandler>
        void asyncReceive(Buffer&& receiveBuffer, CompletionHandler&& handler)
        {
            boost::asio::async_read(asioSocket, receiveBuffer, handler);
        }
    };
}
