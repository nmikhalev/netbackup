#pragma once

#include "PlatformInfo.h"

#ifdef NETBACKUP_LINUX
#define NETBACKUP_PRETTY_FUNCTION __PRETTY_FUNCTION__
#endif

#ifdef NETBACKUP_WINDOWS
#define NETBACKUP_PRETTY_FUNCTION __FUNCTION__
#endif
